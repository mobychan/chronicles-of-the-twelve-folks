using System.Collections.Generic;
using System.IO;
using System.Text;
using Foresoft.UIBase;
using Newtonsoft.Json;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Foresoft.Chronicles.StatSystem
{
    public partial class StatSystemEditor : EditorWindow
    {
        private const string folderPath = "Assets/StatSystem/";
        private const string editorFolderPath = "Assets/StatSystem/Editor/";
        private readonly string fieldSavePath = Path.GetFullPath(Path.Combine(Application.streamingAssetsPath, "Fields"));
        private VisualElement root;
        private Toolbar toolbarRoot;
        private List<Texture> textures = new List<Texture>();
        private FieldArea fieldAreaData = null;

        private string selectedCharacter = newCharacterValue;
        private Field selectedField = null;
        private VisualElement selectedImage = null;

        private JsonSerializerSettings jsonSettings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.None,
        };

        #region ShowExample
        [MenuItem("Window/Foresoft/StatSystemEditor")]
        public static void ShowStatSystemEditor()
        {
            var wnd = GetWindow<StatSystemEditor>();
            wnd.titleContent = new GUIContent("StatSystemEditor");
        }
        #endregion

        #region OnEnable
        public void OnEnable()
        {
            root = rootVisualElement;
            LoadUXMLAndStylesheet();
            toolbarRoot = root.Q<Toolbar>();

            InsertRefreshAndSaveButtons();

            DisplayCharacterSelect();
            DisplayFieldSettingsBar();

            var circleAmount = root.Q<SliderInt>("circleAmount");
            var circleAmountLabel = root.Q<Label>("circleAmountLabel");
            circleAmount.RegisterCallback<ChangeEvent<int>>((evt) =>
            {
                fieldAreaData = new FieldArea(evt.newValue);
                circleAmountLabel.text = evt.newValue.ToString();
                var scroll = root.Q<ScrollView>();
                // var scroll = root.Q<VisualElement>("FieldArea");
                // scroll.style.position = new StyleEnum<Position>(Position.Absolute);
                // scroll.style.top = 0;
                // scroll.style.left = 0;
                // scroll.style.height = 64 * (evt.newValue * 2 + 1) + 32;
                // scroll.style.width = 128 * (evt.newValue * 2 + 1);

                DisplayFields();
            });

            var generateFieldAreaButton = root.Q<ToolbarButton>("generateFieldArea");
            generateFieldAreaButton.RegisterCallback<MouseUpEvent>((evt) =>
            {
                fieldAreaData = new FieldArea(circleAmount.value);
                DisplayFields();
            });
        }
        #endregion

        #region LoadUXMLAndStylesheet
        private void LoadUXMLAndStylesheet()
        {
            var visualTree = UIBaseHelper.LoadAsset<VisualTreeAsset>(editorFolderPath, "StatSystemEditorUXML.uxml");
            root.Add(visualTree.CloneTree());

            var styleSheet = UIBaseHelper.LoadAsset<StyleSheet>(editorFolderPath, "StatSystemEditorStyle.uss");
            root.styleSheets.Add(styleSheet);

            root.styleSheets.Add(UIBaseHelper.BaseStyleSheet);
        }
        #endregion

        #region InsertRefreshAndSaveButtons
        private void InsertRefreshAndSaveButtons()
        {
            toolbarRoot.Insert(0, UIBaseHelper.GetToolbarButton("refreshButton", "Refresh", (evt) =>
            {
                root.Clear();
                OnEnable();
            }));

            toolbarRoot.Insert(1, UIBaseHelper.GetToolbarButton("saveButton", "Save", (evt) =>
            {
                if (!Directory.Exists(fieldSavePath))
                {
                    Directory.CreateDirectory(fieldSavePath);
                }

                if (fieldAreaData != null)
                {
                    var fileName = selectedCharacter == newCharacterValue ? newCharacterName : selectedCharacter;
                    var path = Path.Combine(fieldSavePath, $"{fileName}.json");
                    using (var sw = new StreamWriter(path, false, Encoding.UTF8))
                    {
                        sw.Write(JsonConvert.SerializeObject(fieldAreaData, jsonSettings));
                    }

                    Debug.Log($"Field Area Data saved to {path}.");
                }
                else
                {
                    Debug.Log("No data to save.");
                }
            }));
        }
        #endregion

        #region LoadCurrentFieldArea
        private void LoadCurrentFieldArea()
        {
            var path = Path.Combine(fieldSavePath, $"{selectedCharacter}.json");

            if (File.Exists(path))
            {
                using (var sr = new StreamReader(path, Encoding.UTF8))
                {
                    var jsonString = sr.ReadToEnd();
                    fieldAreaData = JsonConvert.DeserializeObject<FieldArea>(jsonString, jsonSettings);
                }
            }
        }
        #endregion
    }
}