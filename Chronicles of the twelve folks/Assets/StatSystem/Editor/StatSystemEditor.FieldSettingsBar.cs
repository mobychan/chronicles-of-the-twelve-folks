using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Foresoft.Chronicles.StatSystem
{
    public partial class StatSystemEditor
    {
        #region RefreshFieldSettingsBar
        private void RefreshFieldSettingsBar()
        {
            DisplayStatFieldSettings();
            DisplayResistanceFieldSettings();
            DisplayTechniqueFieldSettings();
            DisplayAbilityFieldSettings();
            DisplayTotalAmounts();
        }
        #endregion

        #region DisplayFieldSettingsBar
        private void DisplayFieldSettingsBar()
        {
            RefreshFieldSettingsBar();

            var settingsBar = root.Q<VisualElement>("fieldSettingsBar");
            if (selectedField == null)
            {
                settingsBar.SetEnabled(false);
                return;
            }

            settingsBar.SetEnabled(true);

            var fieldType = root.Q<EnumField>("settingsFieldType");
            fieldType.Init(selectedField.FieldType);
            fieldType.RegisterCallback<ChangeEvent<Enum>>((evt) =>
            {
                selectedImage.RemoveFromClassList($"statField{selectedField.FieldType.GetColor()}");
                selectedImage.RemoveFromClassList($"statField{selectedField.FieldType.GetColor()}Hover");
                selectedField.FieldType = (FieldType)evt.newValue;
                selectedImage.AddToClassList($"statField{selectedField.FieldType.GetColor()}");
                selectedImage.AddToClassList($"statField{selectedField.FieldType.GetColor()}Hover");

                var newSelectedField = GetNewField(selectedField);
                fieldAreaData.ReplaceField(selectedField, newSelectedField);
                selectedField = newSelectedField;

                RefreshFieldSettingsBar();
            });
        }
        #endregion

        #region GetNewField
        private Field GetNewField(Field field)
        {
            switch (field.FieldType)
            {
                default:
                    return new Field(field.CurrentCoords, field.FieldType, field.Selectable);
                case FieldType.Technique:
                    return new TechniqueField(selectedField.CurrentCoords);
                case FieldType.Ability:
                    return new AbilityField(selectedField.CurrentCoords);
                case FieldType.Resistance:
                    return new ResistanceField(selectedField.CurrentCoords);
                case FieldType.Stat:
                    return new StatField(selectedField.CurrentCoords);
            }
        }
        #endregion

        #region Settings by FieldType
        #region DisplayStatFieldSettings
        private void DisplayStatFieldSettings()
        {
            var settingsStats = root.Q<VisualElement>("settingsStats");
            if (selectedField is StatField)
            {
                settingsStats.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
                var statField = selectedField as StatField;

                var statType = root.Q<EnumField>("settingsStatType");
                statType.Init(statField.StatType);
                statType.RegisterCallback<ChangeEvent<Enum>>((evt) =>
                {
                    statField.StatType = (StatType)evt.newValue;
                    RefreshFieldSettingsBar();
                });

                var statValue = root.Q<IntegerField>("settingsStatValue");
                statValue.RegisterCallback<ChangeEvent<int>>((evt) =>
                {
                    statField.StatValue = evt.newValue;
                });
            }
            else
            {
                settingsStats.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
            }
        }
        #endregion

        #region DisplayResistanceFieldSettings
        private void DisplayResistanceFieldSettings()
        {
            var settingsResistance = root.Q<VisualElement>("settingsResistance");
            if (selectedField is ResistanceField)
            {
                settingsResistance.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
                var resistanceField = selectedField as ResistanceField;

                var elementType = root.Q<EnumField>("settingsResistanceElementType");
                elementType.Init(resistanceField.ElementType);
                elementType.RegisterCallback<ChangeEvent<Enum>>((evt) =>
                {
                    resistanceField.ElementType = (ElementType)evt.newValue;
                    RefreshFieldSettingsBar();
                });

                var resistanceValue = root.Q<IntegerField>("settingsResistanceValue");
                resistanceValue.RegisterCallback<ChangeEvent<int>>((evt) =>
                {
                    resistanceField.ResistanceValue = evt.newValue;
                });
            }
            else
            {
                settingsResistance.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
            }
        }
        #endregion

        #region DisplayTechniqueFieldSettings
        private void DisplayTechniqueFieldSettings()
        {
            var settingsTechnique = root.Q<VisualElement>("settingsTechnique");
            if (selectedField is TechniqueField)
            {
                settingsTechnique.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
                var techniqueField = selectedField as TechniqueField;

                var techniqueObject = root.Q<ObjectField>("settingsTechniqueObject");
                techniqueObject.value = techniqueField.Technique ?? CreateInstance<Technique>();

                techniqueObject.RegisterCallback<ChangeEvent<UnityEngine.Object>>((evt) =>
                {
                    techniqueField.Technique = (Technique)evt.newValue;
                });
            }
            else
            {
                settingsTechnique.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
            }
        }
        #endregion

        #region DisplayAbilityFieldSettings
        private void DisplayAbilityFieldSettings()
        {
            var settingsAbility = root.Q<VisualElement>("settingsAbility");
            if (selectedField is AbilityField)
            {
                settingsAbility.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
                var abilityField = selectedField as AbilityField;

                var abilityObject = root.Q<ObjectField>("settingsAbilityObject");
                abilityObject.value = abilityField.Ability ?? CreateInstance<Ability>();

                abilityObject.RegisterCallback<ChangeEvent<UnityEngine.Object>>((evt) =>
                {
                    abilityField.Ability = (Ability)evt.newValue;
                });
            }
            else
            {
                settingsAbility.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
            }
        }
        #endregion
        #endregion

        #region TotalAmounts
        #region DisplayTotalAmounts
        private void DisplayTotalAmounts()
        {
            if (fieldAreaData != null)
            {
                var totalAmountsBorder = root.Q<VisualElement>("totalAmountsBorder");
                totalAmountsBorder.Clear();

                var fieldTypes = Enum.GetValues(typeof(FieldType));

                foreach (FieldType fieldType in fieldTypes)
                {
                    var fields = fieldAreaData.GetFieldsOfType(fieldType);
                    var colorClass = $"statField{fieldType.GetColor()}Color";

                    var border = new VisualElement();
                    border.AddToClassList("titledBorder");
                    border.AddToClassList(colorClass);
                    var label = new Label();
                    label.text = fieldType.ToString();
                    var value = new Label();
                    value.text = fields.Count.ToString();

                    border.Add(label);
                    border.Add(value);
                    totalAmountsBorder.Add(border);

                    switch (fieldType)
                    {
                        case FieldType.Stat:
                            DisplayStatTypeTotalAmounts(fields, totalAmountsBorder, colorClass);
                            break;
                        case FieldType.Resistance:
                            DisplayResistanceTypeTotalAmounts(fields, totalAmountsBorder, colorClass);
                            break;
                    }
                }
            }
        }
        #endregion

        #region DisplayStatTypeTotalAmounts
        private void DisplayStatTypeTotalAmounts(List<Field> fields,
                                                    VisualElement totalAmountsBorder,
                                                    string colorClass)
        {
            var statTypes = Enum.GetValues(typeof(StatType));
            foreach (StatType statType in statTypes)
            {
                var border = new VisualElement();
                border.AddToClassList("titledBorder");
                border.AddToClassList("indent");
                border.AddToClassList(colorClass);
                var label = new Label();
                label.text = statType.ToString();
                var value = new Label();
                value.text = fields.Count((f) => ((StatField)f).StatType == statType).ToString();

                border.Add(label);
                border.Add(value);
                totalAmountsBorder.Add(border);
            }
        }
        #endregion

        #region DisplayResistanceTypeTotalAmounts
        private void DisplayResistanceTypeTotalAmounts(List<Field> fields,
                                                        VisualElement totalAmountsBorder,
                                                        string colorClass)
        {
            var elementTypes = Enum.GetValues(typeof(ElementType));
            foreach (ElementType elementType in elementTypes)
            {
                var border = new VisualElement();
                border.AddToClassList("titledBorder");
                border.AddToClassList("indent");
                border.AddToClassList(colorClass);
                var label = new Label();
                label.text = elementType.ToString();
                var value = new Label();
                value.text = fields.Count((f) => ((ResistanceField)f).ElementType == elementType).ToString();

                border.Add(label);
                border.Add(value);
                totalAmountsBorder.Add(border);
            }
        }
        #endregion
        #endregion
    }
}