using System.Collections.Generic;
using System.IO;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Foresoft.Chronicles.StatSystem
{
    public partial class StatSystemEditor
    {
        private string newCharacterName = string.Empty;
        private static string newCharacterValue = "New";

        #region DisplayCharacterSelect
        private void DisplayCharacterSelect()
        {
            var currentCharacterSelect = root.Q<VisualElement>("currentCharacterSelect");
            var newNameArea = root.Q<VisualElement>("newNameArea");

            var characterList = new List<string> { newCharacterValue };
            var files = Directory.GetFiles(fieldSavePath, "*.json");
            for (var i = 0; i < files.Length; i++)
            {
                characterList.Add(Path.GetFileNameWithoutExtension(files[i]));
            }

            var select = new PopupField<string>(characterList, selectedCharacter);
            currentCharacterSelect.Add(select);
            select.RegisterCallback<ChangeEvent<string>>((evt) =>
            {
                selectedCharacter = evt.newValue;
                if (selectedCharacter == newCharacterValue)
                {
                    newNameArea.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.Flex);
                }
                else
                {
                    newNameArea.style.display = new StyleEnum<DisplayStyle>(DisplayStyle.None);
                }

                LoadCurrentFieldArea();
            });

            var newNameField = root.Q<TextField>("newNameField");
            newNameField.RegisterCallback<ChangeEvent<string>>((evt) => { newCharacterName = evt.newValue; });
        }
        #endregion
    }
}