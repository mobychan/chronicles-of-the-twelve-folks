using System;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UIElements;

namespace Foresoft.Chronicles.StatSystem
{
    public partial class StatSystemEditor
    {
        #region DisplayFields
        private void DisplayFields()
        {
            var fieldArea = root.Q<VisualElement>("FieldArea");
            fieldArea.Clear();

            if (fieldAreaData != null)
            {
                for (var row = -fieldAreaData.CirclesAmount; row <= fieldAreaData.CirclesAmount; row++)
                {
                    var fieldAreaRow = GetFieldAreaRow(fieldArea);
                    for (var col = -fieldAreaData.CirclesAmount; col <= fieldAreaData.CirclesAmount; col++)
                    {
                        var image = new VisualElement();
                        image.AddToClassList("statField");
                        var field = fieldAreaData.GetField(col, row);

                        if ((field != null) && field.Selectable)
                        {
                            var focusArea = new VisualElement();
                            focusArea.style.width = new StyleLength(64);
                            focusArea.style.height = new StyleLength(64);
                            focusArea.style.marginLeft = new StyleLength(32);
                            image.Add(focusArea);
                            var coords = field?.CurrentCoords;
                            focusArea.RegisterCallback<MouseUpEvent>((evt) =>
                            {
                                var innerField = fieldAreaData.GetField(coords);
                                if (selectedField == innerField)
                                {
                                    if (selectedImage != null)
                                    {
                                        selectedImage.RemoveFromClassList($"statField{innerField.FieldType.GetColor()}Hover");
                                    }

                                    selectedField = null;
                                    selectedImage = null;
                                }
                                else
                                {
                                    if (selectedImage != null)
                                    {
                                        var fieldTypes = Enum.GetValues(typeof(FieldType));
                                        foreach (FieldType fieldType in fieldTypes)
                                        {
                                            selectedImage.RemoveFromClassList($"statField{fieldType.GetColor()}Hover");
                                        }
                                    }
                                    selectedField = innerField;
                                    selectedImage = image;
                                    DisplayFieldSettingsBar();
                                }
                            });
                            focusArea.RegisterCallback<MouseEnterEvent>((evt) =>
                            {
                                var innerField = fieldAreaData.GetField(coords);
                                image.AddToClassList($"statField{innerField.FieldType.GetColor()}Hover");
                            });
                            focusArea.RegisterCallback<MouseLeaveEvent>((evt) =>
                            {
                                var innerField = fieldAreaData.GetField(coords);
                                if (innerField != selectedField)
                                {
                                    image.RemoveFromClassList($"statField{innerField.FieldType.GetColor()}Hover");
                                }
                            });
                        }

                        image.AddToClassList(field == null ?
                                                "" : "statField" + field.FieldType.GetColor() +
                                                (field == selectedField ? "Hover" : ""));

                        if ((col & 1) == 0)
                        {
                            image.AddToClassList("statFieldOffset");
                        }

                        fieldAreaRow.Add(image);
                    }
                }
            }
            else
            {
                Debug.Log("fieldAreaData is null!");
            }
        }
        #endregion

        #region GetFieldAreaRow
        private VisualElement GetFieldAreaRow(VisualElement parent)
        {
            var row = new VisualElement();
            row.AddToClassList("fieldAreaRow");

            parent.Add(row);

            return row;
        }
        #endregion
    }
}