using UnityEngine;

namespace Foresoft.Chronicles.StatSystem
{
    [CreateAssetMenu(fileName = "New Field", menuName = "Foresoft/Ability Field")]
    public class Ability : ScriptableObject { }
}