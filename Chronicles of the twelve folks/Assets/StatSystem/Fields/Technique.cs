using UnityEngine;

namespace Foresoft.Chronicles.StatSystem
{
    [CreateAssetMenu(fileName = "New Field", menuName = "Foresoft/Technique Field")]
    public class Technique : ScriptableObject { }
}