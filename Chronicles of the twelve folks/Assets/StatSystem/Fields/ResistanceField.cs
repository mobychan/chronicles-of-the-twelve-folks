namespace Foresoft.Chronicles.StatSystem
{
    public class ResistanceField : Field
    {
        public ElementType ElementType;
        public int ResistanceValue;

        public ResistanceField() { }

        public ResistanceField(Coords coords) : base(coords, FieldType.Resistance, true) { }
    }
}