namespace Foresoft.Chronicles.StatSystem
{
    public class StatField : Field
    {
        public StatType StatType;
        public int StatValue;

        public StatField() { }

        public StatField(Coords coords) : base(coords, FieldType.Stat, true) { }
    }
}