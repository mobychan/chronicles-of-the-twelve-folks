using System;

namespace Foresoft.Chronicles.StatSystem
{
    [Serializable]
    public class Coords
    {
        public int X;
        public int Y;

        public Coords(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Coords operator +(Coords a, Coords b)
        {
            return new Coords(a.X + b.X, a.Y + b.Y);
        }

        public static Coords operator *(Coords a, int multiplier)
        {
            return new Coords(a.X * multiplier, a.Y * multiplier);
        }

        public static bool operator ==(Coords a, Coords b)
        {
            if (object.ReferenceEquals(a, null))
            {
                return object.ReferenceEquals(b, null);
            }
            if (object.ReferenceEquals(b, null))
            {
                return false;
            }
            return (a.X == b.X) && (a.Y == b.Y);
        }

        public static bool operator !=(Coords a, Coords b)
        {
            if (object.ReferenceEquals(a, null))
            {
                return !object.ReferenceEquals(b, null);
            }
            if (object.ReferenceEquals(b, null))
            {
                return true;
            }
            return (a.X != b.X) || (a.Y != b.Y);
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(obj, null))
            {
                return object.ReferenceEquals(this, null);
            }
            if (object.ReferenceEquals(this, null))
            {
                return false;
            }
            return (obj is Coords) ? (this == (Coords)obj) : false;
        }

        public override int GetHashCode()
        {
            return X + Y;
        }

        public static readonly Coords[] Directions = new Coords[] { new Coords(+1, 0),
                                                                    new Coords(+1, -1),
                                                                    new Coords(0, -1),
                                                                    new Coords(-1, 0),
                                                                    new Coords(-1, +1),
                                                                    new Coords(0, +1) };
    }
}