namespace Foresoft.Chronicles.StatSystem
{
    public class Field
    {
        public Coords CurrentCoords { get; private set; }
        public FieldType FieldType { get; set; }
        public bool Selectable { get; private set; }
        public int UnlockCost { get; set; }

        public Field() { }

        public Field(Coords coords, FieldType fieldType, bool selectable)
        {
            CurrentCoords = coords;
            FieldType = fieldType;
            Selectable = selectable;
        }
    }
}