namespace Foresoft.Chronicles.StatSystem
{
    public class AbilityField : Field
    {
        public Ability Ability;

        public AbilityField() { }

        public AbilityField(Coords coords) : base(coords, FieldType.Ability, true) { }
    }
}