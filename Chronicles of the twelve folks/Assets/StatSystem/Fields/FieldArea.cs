using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Foresoft.Chronicles.StatSystem
{
    [Serializable]
    public class FieldArea
    {
        public int CirclesAmount { get; private set; }
        public int Max { get; private set; }
        [JsonProperty]
        private Dictionary<Coords, Field> fields = new Dictionary<Coords, Field>();
        [JsonProperty]
        private Dictionary<int, List<Coords>> rings = new Dictionary<int, List<Coords>>();

        public FieldArea()
        {

        }

        public FieldArea(int circlesAmount)
        {
            Max = circlesAmount * 2 + 1;
            CirclesAmount = circlesAmount;

            for (var circle = 0; circle <= CirclesAmount; circle++)
            {
                var ring = GetCoordsOnCircle(new Coords(0, 0), circle);
                rings.Add(circle, ring);

                for (var i = 0; i < ring.Count; i++)
                {
                    var color = FieldType.None;
                    var selectable = true;
                    if ((ring[i].X == 0) && (ring[i].Y == 0))
                    {
                        color = FieldType.Chara;
                        selectable = false;
                    }
                    else
                    {
                        color = FieldType.White;
                    }

                    fields.Add(ring[i], new Field(ring[i], color, selectable));
                }
            }
        }

        public void ReplaceField(Field original, Field replace)
        {
            fields[original.CurrentCoords] = replace;
        }

        public Field GetField(Coords coords)
        {
            if ((coords != null) && fields.ContainsKey(coords))
            {
                return fields[coords];
            }

            return null;
        }

        public Field GetField(int col, int row)
        {
            var x = col;
            var z = row - (col - (col & 1)) / 2;

            return GetField(new Coords(x, z));
        }

        public List<Field> GetFieldsOfType(FieldType fieldType)
        {
            var result = new List<Field>();
            foreach (var coords in fields.Keys)
            {
                if (fields[coords].FieldType == fieldType)
                {
                    result.Add(fields[coords]);
                }
            }

            return result;
        }

        private bool InRadius(Coords target, int max, int radius)
        {
            var center = new Coords(0, 0);
            if (target == center)
            {
                return true;
            }

            for (var circle = 1; circle <= radius; circle++)
            {
                if (InCircle(center, target, circle))
                {
                    return true;
                }
            }

            return false;
        }

        private bool InCircle(Coords center, Coords target, int circle)
        {
            var field = center + Coords.Directions[4] * circle;
            for (var directionIndex = 0; directionIndex < 6; directionIndex++)
            {
                for (var sideIndex = 0; sideIndex < circle; sideIndex++)
                {
                    if (field == target)
                    {
                        return true;
                    }
                    field += Coords.Directions[directionIndex];
                }
            }

            return false;
        }

        private List<Coords> GetCoordsOnCircle(Coords center, int circle)
        {
            if (circle == 0)
            {
                return new List<Coords> { center };
            }

            var coords = new List<Coords>();
            var field = center + Coords.Directions[4] * circle;
            for (var directionIndex = 0; directionIndex < 6; directionIndex++)
            {
                for (var sideIndex = 0; sideIndex < circle; sideIndex++)
                {
                    field += Coords.Directions[directionIndex];
                    coords.Add(field);
                }
            }

            return coords;
        }
    }
}