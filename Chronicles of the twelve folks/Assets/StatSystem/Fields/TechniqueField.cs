namespace Foresoft.Chronicles.StatSystem
{
    public class TechniqueField : Field
    {
        public Technique Technique;

        public TechniqueField() { }

        public TechniqueField(Coords coords) : base(coords, FieldType.Technique, true) { }
    }
}