namespace Foresoft.Chronicles.StatSystem
{
    public enum ElementType
    {
        None,
        Fire,
        Ice,
        Wind,
        Water
    }
}