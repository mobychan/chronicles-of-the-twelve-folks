namespace Foresoft.Chronicles.StatSystem
{
    public enum StatType
    {
        None,
        HP,
        MP,
        Attack,
        Defense,
    }
}