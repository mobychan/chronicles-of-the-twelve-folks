namespace Foresoft.Chronicles.StatSystem
{
    public enum FieldType
    {
        None,
        Chara,
        White,
        Technique,
        Ability,
        Stat,
        Resistance
    }

    public static class FieldTypeExt
    {
        public static string GetColor(this FieldType fieldType)
        {
            switch (fieldType)
            {
                default:
                    return "None";
                case FieldType.Chara:
                    return "Chara";
                case FieldType.White:
                    return "White";
                case FieldType.Technique:
                    return "Red";
                case FieldType.Ability:
                    return "Blue";
                case FieldType.Stat:
                    return "Green";
                case FieldType.Resistance:
                    return "Yellow";
            }
        }
    }
}